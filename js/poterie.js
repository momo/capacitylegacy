/**
 * Created by momo on 18/03/15.
 */
/*
 poterie.js
 ----------
 Fichier permettant de gérer l'affichage et effectuer le calcul du volume d'un profil pour le site web capacité
 Auteur : Rudy Ercek @ LISA (http://lisa.ulb.ac.be)
 Année  : 2013
 */
/*variables définies dans le fichier html du canvas :
 - imgFile : nom du fichier image à afficher (en rajoutant l'extension txt, on obtient le fichier profil)
 - complete : information sur le type de profil : gauche (0) ou droit (1) de l'image 0 ou 1
 - lang : langue d'affichage : en ou fr
 */
var canvas = document.getElementById('poterie'); //élément principal dans lequel le dessin sera réalisé
var ctx = canvas.getContext('2d'); //contexte du canvas
var canv_minw=512; //largeur minimale du canvas moins les bords (i.e. largeur minimale de l'image redimensionnée)
var canv_minh=384; //hauteur minimale du canvas moins les bords (i.e. hauteur minimale de l'image redimensionnée)
var canv_border_right=80; //bord droit (pour dessinner les sliders)
var canv_border_down=10;  //bord inférieur (pour aérer un peu le dessin et permettre l'affichage correct des sliders)
var canv_border_up=10;    //bord supérieur (...)
var canv_ratio=0.6;       //rapport entre la zone visible du navigateur et le canvas (en tenant compte de la taille minimale)
var mingrid=20;           //nombre minimal de pixels pour l'écart entre chaque droite de la grille
var img_ratio;			  //rapport entre la taille de l'image affichée et la taille réelle de celle-ci
var img = new Image();    //image du profil
var file=imgfile+".txt";  //nom du fichier texte contenant le profil
var xAxe;                 //valeur du centre de l'axe
var xList=new Array();    //coordonnées x du profil (vecteur)
var yList=new Array();    //coordonnées y du profil (vecteur)
var miny=65000;           //valeur minimale de y correspondant au niveau haut d'eau
var maxy=0;               //valeur maximale de y correspondant au niveau bas d'eau
var hwater=0;             //hauteur d'eau (écart entre les deux niveaux)
var scale=1;			  //échelle : 1 cm sur le dessin vaut "scale" cm (en vrai)
var flag_clicked=0;		  //type de clic (=1 près de l'axe haut miny et =2 près de l'axe bas maxy)
var dpix=8;				  //distance maximale pour être considérée comme un clic près d'un niveau d'eau
var isloaded = false;     //vrai si le texte et l'image ont bien été chargées !
var indminx = 0;          //indice dans le vecteur où se trouve le x minimum
//variables de sauvegardes pour reset (concaténation de save et du nom)
var saveminy=0;
var savemaxy=0;
var savedpi=dpi;		  //nombre de dpi de l'image en pixels/m
var savexList;
var saveyList;
//variables de messages (alert)
var dpipos="La valeur DPI doit être un nombre entier positif !";
var hpos="La valeur d'hauteur d'eau doit être un nombre positif !";
var scalepos="L'échelle doit être un nombre positif !";
var questdpi="Voulez-vous modifier la valeur DPI _OLDDPI_ avec la nouvelle valeur _NEWDPI_ (_NEWDPICM_ pixels/cm) ?";
var questh="La modification de la valeur d'hauteur d'eau provoquera une modification de l'échelle du dessin d'un facteur _FACTEUR_. Après confirmation, une mesure de _H_ cm correspondra donc à une mesure de _NEWH_ cm. Voulez-vous continuer ?";
if (lang=="en") {
    dpipos="DPI must be a positive integer !";
    hpos="Water height must be a positive integer !";
    scalepos="Scale must be a positive integer !";
    questdpi="Do you want to modify the DPI _OLDDPI_ with the new value _NEWDPI_ (_NEWDPICM_ pixels/cm) ?";
    questh="The modification of water height will modify the drawing scale of a factor _FACTEUR_. After confirmation, a measure of _H_ cm will correspond to a measure of _NEWH_ cm. Do you want to continue ?";
}

//chargement du fichier texte contenant le profil
var imgFile = new XMLHttpRequest();
imgFile.open("GET", file, true);
imgFile.onreadystatechange = loadTxt;
imgFile.send(null);
window.onresize = function() {
    if (isloaded) initializeCanvas();
}

//callback sur le canvas
canvas.addEventListener('click', function(event) {treatMouseClick(event);} );
canvas.addEventListener('mousedown', function(event) {treatMouseDown(event);} );
canvas.addEventListener('mousemove', function(event) {treatMouseMove(event);} );
canvas.addEventListener('mouseout', function(event) {treatMouseOut(event);} );
//callback sur les edit box de la page
document.getElementById('dpi').addEventListener('keydown', function(event) {treatKeydownDPI(event);} );
document.getElementById('hwater').addEventListener('keydown', function(event) {treatKeydownH(event);} );
document.getElementById('scale').addEventListener('keydown', function(event) {treatKeydownS(event);} );

//fonction callback sur la modification de la valeur DPI (edit)
function treatKeydownDPI(e) {
    if (e.keyCode == 13) //touche ENTER (validation de la valeur entrée)
    {
        //la valeur dpi donnée est en pixels/metre (et on l'affiche en pixels par pouce afin que l'utilisateur puisse facilement le vérifier sur les propriétés de l'image)
        var newdpi=parseInt(document.getElementById('dpi').value);
        var olddpi=(dpi*0.0254).toFixed();
        if (isNaN(newdpi) || newdpi<=0)
        {
            alert(dpipos);
            document.getElementById('dpi').value=olddpi;
        }
        else
        {
            if (olddpi!=newdpi.toFixed())
            {
                var newdpim=Math.round(newdpi/2.54*100);
                var q=questdpi.replace("_OLDDPI_",olddpi).replace("_NEWDPI_",newdpi.toFixed()).replace("_NEWDPICM_",(newdpim/100).toFixed(2));
                //var q="Voulez-vous modifier la valeur DPI ("+olddpi+") avec la nouvelle valeur "+newdpi.toFixed()+" ("+(newdpim/100).toFixed(2)+" pixels/cm)?";
                if (confirm(q))
                {
                    dpi=newdpim;
                    if (isloaded) initializeCanvas();
                }
                else document.getElementById('dpi').value=olddpi;
            }
        }
    }
}

//fonction callback sur la modification de la valeur d'hauteur --> adaptation de l'échelle (scale)
function treatKeydownH(e) {
    if (e.keyCode == 13) //touche ENTER (validation de la valeur entrée)
    {
        var newhwater=parseFloat(document.getElementById('hwater').value);
        if (isNaN(newhwater) || newhwater<=0)
        {
            alert(hpos);
            document.getElementById('hwater').value=hwater.toString(1);
        }
        else
        {
            var fact=newhwater/hwater;
            var q=questh.replace("_FACTEUR_",fact.toFixed(3)).replace("_H_",hwater.toFixed(1)).replace("_NEWH_",newhwater.toFixed(1));
            //var q="La modification de la valeur d'hauteur d'eau provoquera une modification de l'échelle du dessin d'un facteur "+fact.toFixed(3)+". Après confirmation, une mesure de "+hwater.toFixed(1)+" cm correspondra donc à une mesure de "+newhwater.toFixed(1)+" cm. Voulez-vous continuer ?";
            if (confirm(q))
            {
                scale*=fact;
                if (isloaded) initializeCanvas();
            }
            else document.getElementById('hwater').value=hwater.toString(1);
        }
    }
}

//fonction callback sur la modification de la valeur du scale (i.e. de l'échelle)
function treatKeydownS(e) {
    if (e.keyCode == 13) //touche ENTER (validation de la valeur entrée)
    {
        var newscale=parseFloat(document.getElementById('scale').value);
        if (isNaN(newscale) || newscale<=0)
        {
            alert(scalepos);
            document.getElementById('scale').value=scale.toFixed(3);
        }
        else
        {
            scale=newscale;
            if (isloaded) initializeCanvas();
        }
    }
}

//fonction callback du canvas lorsqu'on presse le bouton gauche de la souris dessus
function treatMouseDown(e) {
    if(!isloaded) return;
    var offX = (e.offsetX || e.layerX - e.originalTarget.offsetLeft);// Chrome+IE || Firefox_circle
    var offY = (e.offsetY || e.layerY - e.originalTarget.offsetTop);
    if (Math.abs(offY-getY(miny))<dpix) flag_clicked=1;  //on vérifie qu'on est près du y minimum (i.e. niveau haut)
    else if (Math.abs(offY-getY(maxy))<dpix) flag_clicked=2; //on vérifie qu'on est près du y maximum (i.e. niveau bas)
}

//fonction callback du canvas lorsqu'on bouge la souris dessus
function treatMouseMove(e) {
    var offY = (e.offsetY || e.layerY - e.originalTarget.offsetTop);
    if (flag_clicked==1) //on bouge l'axe de niveau haut
    {
        var newminy=offY/img_ratio-canv_border_up;
        if (newminy<maxy && newminy>=0)
        {
            miny=newminy;
            initializeCanvas();
        }
    }
    else if (flag_clicked==2) //on bouge l'axe de niveau bas
    {
        var newmaxy=offY/img_ratio-canv_border_up;
        if (newmaxy>miny && newmaxy<=img.height)
        {
            maxy=newmaxy;
            initializeCanvas();
        }

    }

}

//fonction callback du canvas lorsqu'on relache le bouton gauche de la souris
function treatMouseOut(e) {
    flag_clicked=0;
}

//fonction callback du canvas lorsqu'on clique sur celui-ci
function treatMouseClick(e) {
    flag_clicked=0;
}

//fonction de réinitialisation de toutes les valeurs aux valeurs de chargement de la page
function reset() {
    if (!isloaded) return;
    miny=saveminy;
    maxy=savemaxy;
    dpi=savedpi;
    xList=savexList.slice(0);
    yList=saveyList.slice(0);
    scale=1;
    initializeCanvas();
}

//fonction pour soumettre le formulaire de "volume incorrect"
function sendform(url,titre) {
    /*propriete = "top=0,left=0,resizable=yes, status=no, directories=no, addressbar=no, toolbar=no, scrollbars=yes, menubar=no, location=no, statusbar=no";
     propriete += ",width=" + screen.availWidth + ",height=" + screen.availHeight;
     win = window.open(url,titre, propriete);
     return false;*/
    var w=600;
    var h=400;
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    return window.open(url, titre, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

//fonction appelée lorsque le fichier contenant le profil a été chargé
function loadTxt() {
    if(imgFile.readyState === 4)
    {
        if(imgFile.status === 200 || imgFile.status == 0)
        {
            var allText = imgFile.responseText;
            parseTxt(allText);
        }
    }
}

//fonction qui parse le fichier de profil
function parseTxt(txt) {
    var lines = txt.split("\n");
    var numLines = lines.length;
    if (numLines>1) //on vérifie que le fichier contient plus d'une ligne !
    {
        xAxe=parseInt(lines[0]); //extraction du centre de l'axe
        for (var i=1;i<numLines;i++) { //extractiond des coordonnées de profil
            var val=lines[i].split(" ");
            if (val.length==2) {
                var x=parseInt(val[0]);
                var y=parseInt(val[1]);
                xList.push(x);
                yList.push(y);
                maxy=Math.max(y,maxy);
                miny=Math.min(y,miny);
            }
        }
        //sauvegarde des différentes valeurs pour réinitialisation !
        saveminy=miny;
        savemaxy=maxy;
        savexList=xList.slice();
        saveyList=yList.slice();
        img.onload = initializeCanvas;
        img.src=imgfile;
    }
}

//fonction analysant le profil et identifiant si le premier point du profil est bien le proche de l'axe de révolution (màj indmin qui permet de savoir si le profil peut être corrigée (cf. retour)
function analyzeProfil()
{
    var lg = xList.length;
    var xmin=xList[0];
    indminx=0;
    for (var i=0;i<lg;i++)
    {
        if (xList[i]<xmin || (xmin==xList[i] && yList[i]>yList[indminx]))
        {
            xmin=xList[i];
            indminx=i;
        }
    }
    //on active ou nom le bouton de correctoin auto du profil !
    if (indminx) document.getElementById("correctBtn").disabled=false;
    else document.getElementById("correctBtn").disabled=true;
}

//correction en coupant une partie du profil (si il y a une anomalie)
function CorrectProfil() {
    if (indminx) //anomalie détectée ... --> on corrige ...
    {
        xList=xList.slice(indminx);
        yList=yList.slice(indminx);
        initializeCanvas();
        //alert("Une correction automatique du profil a été appliquée mais sans garantie d'amélioration ! ");
    }
}

//initialisation du canvas avec le dessin de la poterie et le reste ...
function initializeCanvas() {
    //obtain width and height
    isloaded=true;
    var w=window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var h=window.innerHeight || document.documentElement.clientHeigt || document.body.clientHeight;
    //adaptation de la taille du canvas en fonction de la taille de l'image
    w=Math.max(canv_ratio*w,canv_minw);
    h=Math.max(canv_ratio*h,canv_minh);
    img_ratio=Math.min(w/img.width,h/img.height);
    var imgw=Math.floor(img.width*img_ratio);
    var imgh=Math.floor(img.height*img_ratio);
    canvas.width=imgw+canv_border_right;
    canvas.height=imgh+canv_border_down+canv_border_up;
    ctx.drawImage(img,0,canv_border_up,imgw,imgh);	//on dessine l'image à une taille adaptée
    analyzeProfil();//Analyse du profil (pour savoir s'il y a un retour)
    drawGrid(); 	//Dessin de la grille
    drawAxe(); 		//Dessin de l'axe
    drawProfil();	//Dessin du profil
    drawLevels();   //Dessin des niveaux d'eaux (slider)
    drawWater();    //Dessin de l'eau + calcul du volume !!!
}

//fonction qui dessine l'axe (droite verticale en rouge)
function drawAxe() {
    ctx.beginPath();
    ctx.strokeStyle = 'rgb(255, 0, 0)';
    ctx.lineWidth=2;
    ctx.moveTo(xAxe*img_ratio,getY(0));
    ctx.lineTo(xAxe*img_ratio,getY(img.height));
    ctx.stroke();
}

//fonction qui dessine le profil (en vert)
function drawProfil() {
    var lg=xList.length;
    var xoffset=xAxe*img_ratio;;
    ctx.beginPath();
    ctx.strokeStyle = 'rgb(0, 255, 0)';
    ctx.lineWidth=2;
    ctx.moveTo(getX(xList[0]),getY(yList[0]));
    for (var i=1;i<lg;i++) {
        ctx.lineTo(getX(xList[i]),getY(yList[i]));
    }
    ctx.stroke();
}

//fonction qui dessine la grille
function drawGrid() {
    var nbe=(dpi/scale)/100*img_ratio;
    var valcm=1; //par défaut une grille d'1cm
    var w=img.width*img_ratio;
    var h=getY(img.height);
    if (nbe<mingrid) //une grille d'1cm est trop petite --> adaptation de la taille !
    {
        valcm=Math.ceil(mingrid/nbe);
        nbe=valcm*nbe;
    }
    ctx.beginPath;
    for (var x=0;x<w;x+=nbe) //dessin des lignes verticales de la grille
    {
        var xx=Math.round(x)+0.5;
        ctx.moveTo(xx,getY(0));
        ctx.lineTo(xx,h);
    }
    for (var y=h;y>=getY(0);y-=nbe) //dessin des lignes horizontales de la grille
    {
        var yy=Math.round(y)+0.5;
        ctx.moveTo(0,yy);
        ctx.lineTo(w,yy);
    }
    ctx.strokeStyle='rgba(0,0,0,.1)'; //grille noire très transparente (alpha à 10%)
    ctx.stroke();
    ctx.fillText(valcm.toFixed()+"cm",0,h); //imprime la valeur des côtés d'un carrés de la grille
}

//fonction qui dessine les sliders de niveau d'eau
function drawLevels() {
    ctx.beginPath();
    ctx.strokeStyle = 'rgb(0, 0, 255)';
    ctx.lineWidth=1;
    var w=img.width*img_ratio+10;
    ctx.moveTo(0,getY(maxy));
    ctx.lineTo(w,getY(maxy));
    ctx.lineTo(w,getY(miny));
    ctx.lineTo(0,getY(miny));
    ctx.stroke();
    drawCursor(w+4,getY(maxy),ctx.strokeStyle);
    drawCursor(w+4,getY(miny),ctx.strokeStyle);
    var ymean=(maxy+miny)/2;
    var h=(maxy-miny);
    hwater=100*h/(dpi/scale); //calcul de la hauteur d'eau
    var hh=hwater.toFixed(1);
    //on ajoute le texte des hauteurs
    ctx.font = "bold 12px sans-serif";
    ctx.fillText(hh+" cm",w+5,getY(ymean)+6);
    ctx.fillText((100*(savemaxy-maxy)/(dpi/scale)).toFixed(1)+" cm",w+7,getY(maxy)+6);
    ctx.fillText((100*(savemaxy-miny)/(dpi/scale)).toFixed(1)+" cm",w+7,getY(miny)+6);
    document.getElementById('hwater').value=hh;
}

//fonction qui dessine un petit triangle correspondant au curseur <|
function drawCursor(x,y,style)
{
    var lg=16; //longueur du curseur
    var e=6;   //hauteur du curseur
    ctx.beginPath();
    ctx.fillStyle=style;
    ctx.strokeStyle=style;
    ctx.moveTo(x-lg,y);
    ctx.lineTo(x,y+e);
    ctx.lineTo(x,y-e);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
}

//fonction qui dessine le remplissage d'eau ET calcule le volume
function drawWater() {
    var lg=xList.length;
    var i=0;
    var xfirst=-1,yfirst; //coordonnées du premier point dans l'eau
    var xlast,ylast;  //coordonnée du dernier point dans l'eau
    var volpix=0;    //volume en pixels
    var str="";      //string ayant servi pour debug (on pourrait l'effacer)
    ctx.beginPath();
    ctx.fillStyle = 'rgba(45, 180, 255, 0.5)';
    ctx.lineWidth=2;
    if (maxy<=miny) return 0; //on vérifie que les niveaux sont bien dans le bon sens (et pas identiques)
    //on teste si le premier point de la courbe est à l'intérieur de l'eau
    if (yList[i]>=miny && yList[i]<=maxy) {
        xfirst=xList[i];
        yfirst=yList[i];
    }
    i++;
    for (;i<lg && xfirst==-1;i++) { //on cherche le premier point qui est dans l'eau
        var dy=yList[i]-yList[i-1];
        var dx=xList[i]-xList[i-1];
        if (dy<0 && yList[i]<=maxy && yList[i-1]>=maxy) //on rentre dans le volume d'eau par le bas
        {
            yfirst=maxy;
            xfirst=dx/dy*(yfirst-yList[i])+xList[i]; //interpolation
            if (yList[i]!=maxy) i--;
        }
        if (dy>0 && yList[i]>=miny && yList[i-1]<=miny) //on rentre dans le volume d'eau par le haut
        {
            yfirst=miny;
            xfirst=dx/dy*(yfirst-yList[i])+xList[i]; //interpolation
            if (yList[i]!=miny) i--;
        }
    }
    if (i<lg) {
        //on démarre le dessin du polygone au premier point qui sera rempli par de l'eau
        ctx.moveTo(getX(xfirst),getY(yfirst));
        var inwater = true;
        xlast=xfirst;
        ylast=yfirst;
        str+=xlast.toString()+" "+ylast.toString()+" first<br/>";
        for (;i<lg;i++) {
            var dy=yList[i]-yList[i-1];
            var dx=xList[i]-xList[i-1];
            var xprev=xlast; //derniers points où on était dans l'eau
            var yprev=ylast;
            if (inwater) //on est dans l'eau
            {
                if (yList[i]>=miny && yList[i]<=maxy) { //on est encore dans l'eau
                    xlast=xList[i];
                    ylast=yList[i];
                    ctx.lineTo(getX(xlast),getY(ylast));
                    volpix+=getV(xprev,yprev,xlast,ylast);
                    str+=xlast.toString()+" "+ylast.toString()+" inwater<br/>";
                }
                else //on sort de l'eau
                {
                    if (dy<0 && yList[i]<miny) //on sort par le haut
                    {
                        ylast=miny;
                        xlast=dx*(ylast-yList[i])/dy+xList[i]; //interpolation
                    }
                    if (dy>0 && yList[i]>maxy)	//on sort par le bas
                    {
                        ylast=maxy;
                        xlast=dx*(ylast-yList[i])/dy+xList[i];	//interpolation
                    }
                    ctx.lineTo(getX(xlast),getY(ylast));
                    volpix+=getV(xprev,yprev,xlast,ylast);
                    inwater=false; //on est plus dans l'eau car on est sorti
                    str+=xlast.toString()+" "+ylast.toString()+" inwater out "+i.toString()+" "+dx.toString()+" "+dy.toString()+"<br/>";
                }
            }
            else //on est en dehors du volume d'eau
            {
                if (dy<0 && yList[i]<=maxy && yList[i-1]>=maxy) //on rentre dans le volume d'eau par le bas
                {
                    ylast=maxy;
                    xlast=dx/dy*(ylast-yList[i])+xList[i];
                    ctx.lineTo(getX(xlast),getY(ylast));
                    volpix+=getV(xprev,yprev,xlast,ylast);
                    if (yList[i]!=maxy) i--; //ce point est dans l'eau ou peut être ressorti de l'eau par le haut (pour continuer à tracer) (condition inwater de sortie pourra le vérifier)
                    inwater=true; //on est à nouveau dans l'eau
                    str+=xlast.toString()+" "+ylast.toString()+" in bas<br/>";
                }
                if (dy>0 && yList[i]>=miny && yList[i-1]<=miny) //on rentre dans le volume d'eau par le haut
                {
                    ylast=miny;
                    xlast=dx/dy*(ylast-yList[i])+xList[i];
                    ctx.lineTo(getX(xlast),getY(ylast));
                    volpix+=getV(xprev,yprev,xlast,ylast);
                    if (yList[i]!=miny) i--; //ce point est dans l'eau ou peut être ressorti de l'eau par le bas (pour continuer à tracer) (condition inwater de sortie pourra le vérifier)
                    inwater=true;
                    str+=xlast.toString()+" "+ylast.toString()+" in haut<br/>";
                }
            }
        }
        if (ylast<yfirst) //on trace le polygone en passant par l'axe avant de la fermer
        {
            ctx.lineTo(getX(1/img_ratio),getY(ylast));
            ctx.lineTo(getX(1/img_ratio),getY(yfirst));
        }
        ctx.closePath(); //on fermer le polygone
        ctx.fill();      //on le remplit d'eau
        //mises à jours des valeurs edit et autre
        document.getElementById('dpi').value=(dpi*0.0254).toFixed();
        document.getElementById('dpicm').innerHTML="("+(dpi/100).toFixed(2)+" pixels/cm)";
        document.getElementById('scale').value=scale.toFixed(3);
        document.getElementById('volume').innerHTML="Volume = "+(1000*volpix/((dpi/scale)*(dpi/scale)*(dpi/scale))).toFixed(4)+" L";
        //document.getElementById('txtout').innerHTML=str; //debug
    }
}

//retourne la coordonnée x pour un profil (uniquement)
function getX(x) {
    var xo=xAxe*img_ratio;
    return (complete==1?xo+x*img_ratio:xo-x*img_ratio);
}

//retourne la coordonnée y (pour tout)
function getY(y) {
    return y*img_ratio+canv_border_up;
}

//retourne un élément de volume correspondant à un cône tronqué (avec le signe)
function getV(r1,h1,r2,h2) {
    var dh=-(h2-h1);
    return Math.PI*dh*(r1*r1+r2*r2+r1*r2)/3;
}